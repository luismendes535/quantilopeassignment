const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const MatrixSchema = new Schema({
  columns: [
    {
      label: String,
      fileName: String
    }
  ],
  rows: [
    {
      label: String,
      value: Number,
      fileName: String
    }
  ],
  title: String
});

module.exports = mongoose.model("Matrix", MatrixSchema);
