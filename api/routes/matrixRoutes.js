const MatrixModel = require('../models/Matrix');
const {mongoURI} = require('../config/keys');
const multer = require('multer');
const path = require('path');
const crypto = require('crypto');
const GridFsStorage = require('multer-gridfs-storage');
const Grid = require('gridfs-stream');
const mongoose = require('mongoose')

const conn = mongoose.createConnection(mongoURI, { useNewUrlParser: true });

  // Init gfs
  let gfs;

  conn.once('open', () => {
    // Init stream
    gfs = Grid(conn.db, mongoose.mongo);
    gfs.collection('files');
  });

  // Create storage engine
  const storage = new GridFsStorage({
    url: mongoURI,
    file: (_req, file) => {
      return new Promise((resolve, reject) => {
        crypto.randomBytes(16, (err, buf) => {
          if (err) {
            return reject(err);
          }
          const filename = buf.toString('hex') + path.extname(file.originalname);
          const fileInfo = {
            filename: filename,
            bucketName: 'files'
          };
          resolve(fileInfo);
        });
      });
    }
  });

  const upload = multer({ storage });

module.exports = app => {
  app.get("/matrix", (_req, res)=>{
    MatrixModel.find({}, (err, matrix)=>{
      if(err) return err;
      res.send(matrix);
    })
  });

  app.post("/matrix", (req, res)=>{
    MatrixModel.create({...req.body}, (err, matrix)=>{
      if(err) return err;
      res.send(matrix);
    })
  });
  
  //Upload files
  app.post('/upload', upload.single('file'), (_req, res) => {
    res.redirect('/');
  });
  
}