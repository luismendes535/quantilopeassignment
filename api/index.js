const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const keys = require("./config/keys");
const app = express();
const PORT = process.env.PORT || 5000;

mongoose.Promise = global.Promise;

mongoose.connect(keys.mongoURI, {useNewUrlParser: true}, err => {
    if(err){
       console.log(err);
       return;
    }
    console.log('Connected to database')
});

app.use(bodyParser.json());

require("./routes/matrixRoutes")(app);

app.listen(PORT, () => console.log("Server running on port 5000"));
