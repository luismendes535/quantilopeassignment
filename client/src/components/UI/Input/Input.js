import React from "react";
import "./Input.css";

const Input = ({ type, value, handleChangeInput }) => {
  let inputClasses = [type];
  let input = null;
  switch (type) {
    case "file":
      input = (
        <input
          className={["Input", inputClasses].join(" ")}
          type={type}
          onChange={handleChangeInput}
        />
      );
      break;
    case "radio":
      input = (
        <input
          className={["Input", inputClasses].join(" ")}
          type={type}
          onChange={handleChangeInput}
        />
      );
      break;
    default:
      input = (
        <input
          className="Input"
          type={type}
          value={value}
          onChange={handleChangeInput}
        />
      );
  }

  return (
    <div>
      {input}
    </div>
  );
};

export default Input;
