import React from 'react'
import './Button.css'

const Button=(props)=>{
let inputClasses = [props.type]

  return (
    <div onClick={props.clicked} className={["Button", inputClasses].join(' ')}>
      {props.children}
    </div>
  )
}


export default Button

