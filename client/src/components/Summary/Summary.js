import React from "react";
import PropTypes from "prop-types";
import "./Summary.css";

function Summary(props) {

  const longestLabel = (labels)=>{
    let length=0;
    labels.map(el=>{
      if(el.label.length > length){
        length = el.label.length
      }
    });
    return length;
  }
  const numberOfUploads = (rows, columns)=>{
    let counter = 0;
    rows.map(el=>el.fileName && el.fileName.length>0 ? counter++: null)
    columns.map(el=>el.fileName && el.fileName.length>0 ? counter++: null)
    return counter;
  }
  return (
    <div className="Summary">
      <h3>Summary</h3>
      <p>Number of rows: {props.rows.length}</p>
      <p>Number of columns: {props.columns.length}</p>
      <p>Number of images uploaded: {numberOfUploads(props.rows, props.columns)}</p>
      <p>Longest row label: {longestLabel(props.rows)} </p>
      <p>Longest column label: {longestLabel(props.columns)} </p>
    </div>
  );
}

Summary.propTypes = {};

export default Summary;
