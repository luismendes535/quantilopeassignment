import React, {Component} from "react";
import PropTypes from "prop-types";
import Input from '../UI/Input/Input';
import Button from '../UI/Button/Button';

class Table extends Component{
  render(){
    return (
        <div>
          <caption>
            <Input
              defaultValue={`Title of the question`}
              handleChangeInput={this.props.handleChangeTitle}
              value={this.props.title}
            />
          </caption>
          <table className="Table">
            <tr>
              <td />
              <td ><Button type="Remove" clicked={() => this.props.removeCol()}>-</Button></td>
              {this.props.columns.map((el, id) => (
                  <th>
                  <Button type="Upload" clicked={()=>this.props.openInputModal('columns',id)}>+</Button>
                </th>
              ))}
              <th>
                <Button type="Add" clicked={() => this.props.addCol()}>+</Button>
              </th>
            </tr>
            <tr>
              <td ><Button type="Remove" clicked={() => this.props.removeRow()}>-</Button></td>
              <td />
              {this.props.columns.map((el, id) => (
                <th>
                  <Input
                    handleChangeInput={e=>this.props.handleChangeInput(e, 'columns',id)}
                    value={el.label}
                  />
                </th>
              ))}
            </tr>
            {this.props.rows.map((row, rowId) => (
              <tr>
                <td><Button type='Upload' clicked={() => this.props.openInputModal('rows',rowId)}>+</Button></td>
                <th>
                <Input
                    handleChangeInput={e=>this.props.handleChangeInput(e, 'rows',rowId)}
                    value={row.label}
                  />
                </th>
                {this.props.columns.map((col, colId) => (
                  <th>
                    <Input checked handleChangeInput={()=>this.props.handleAnswer(colId, rowId)} type="radio" />
                  </th>
                ))}
              </tr>
            ))}
            <th>
              <Button type='Add' clicked={() => this.props.addRow()}>+</Button>
            </th>
          </table>
          <Button type='Create' clicked={this.props.createEntry}>Create</Button>
        </div>
      );
  }
}

Table.propTypes = {};

export default Table;
