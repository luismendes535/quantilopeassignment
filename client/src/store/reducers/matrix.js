import * as actionTypes from "../actions/actionTypes";

const initialState = {
  rows: [],
  columns: [],
  file: [],
  error: null,
  loading: false
};

const fetchMatrixRequest = state => {
  return {...state,  error: null, loading: true };
};

const fetchMatrixSuccess = (state, action) => {
  return {...state, 
    rows: action.rows,
    columns: action.columns,
    error: null,
    loading: false
  };
};

const fetchMatrixFail = (state, action) => {
  return { ...state, error: action.error, loading: false };
};
const createMatrixRequest = state => {
  return {...state,  error: null, loading: true };
};

const createMatrixSuccess = (state, action) => {
  return {...state, 
    rows: action.rows,
    columns: action.columns,
    error: null,
    loading: false
  };
};

const createMatrixFail = (state, action) => {
  return { ...state, error: action.error, loading: false };
};

const createFileRequest = state => {
  return {...state,  error: null, loading: true };
};

const createFileSuccess = (state, action) => {
  return {...state, 
    file: action.file,
    error: null,
    loading: false
  };
};

const createFileFail = (state, action) => {
  return { ...state, error: action.error, loading: false };
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FETCH_MATRIX_START:
    //Fetch matrix
      return fetchMatrixRequest(state, action);
    case actionTypes.FETCH_MATRIX_SUCCESS:
      return fetchMatrixSuccess(state, action);
    case actionTypes.FETCH_MATRIX_FAIL:
      return fetchMatrixFail(state, action);
      //Create matrix
    case actionTypes.CREATE_MATRIX_START:
      return createMatrixRequest(state, action);
    case actionTypes.CREATE_MATRIX_SUCCESS:
      return createMatrixSuccess(state, action);
    case actionTypes.CREATE_MATRIX_FAIL:
      return createMatrixFail(state, action);
    //Create file
    case actionTypes.CREATE_FILE_START:
      return createFileRequest(state, action);
    case actionTypes.CREATE_FILE_SUCCESS:
      return createFileSuccess(state, action);
    case actionTypes.CREATE_FILE_FAIL:
      return createFileFail(state, action);
    default:
      return state;
  }
};

export default reducer;
