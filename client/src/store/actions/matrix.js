import axios from 'axios';
import * as actionType from "./actionTypes";

const fetchMatrixStart = () => {
    return {
        type: actionType.FETCH_MATRIX_START
    };
};
const fetchMatrixSuccess = matrixInfo => {
    return {
        type: actionType.FETCH_MATRIX_SUCCESS,
        matrixInfo
    };
};
const fetchMatrixFail = error => {
    return {
        type: actionType.FETCH_MATRIX_FAIL,
        error
    };
};

export const fetchMatrixInfo = () => {
    return dispatch => {
        dispatch(fetchMatrixStart());
        axios.get('/api/matrix')
            .then(data=>{
                dispatch(fetchMatrixSuccess(data))
            })
            .catch(err=>{
                dispatch(fetchMatrixFail(err))
            })
    };
}
const createEntryStart = () => {
    return {
        type: actionType.CREATE_MATRIX_START
    };
};
const createEntrySuccess = matrixInfo => {
    return {
        type: actionType.CREATE_MATRIX_SUCCESS,
        matrixInfo
    };
};
const createEntryFail = error => {
    return {
        type: actionType.CREATE_MATRIX_FAIL,
        error
    };
};

export const createEntry = (payload) => {
    return dispatch => {
        dispatch(createEntryStart());
        axios.post('/api/matrix', payload)
            .then(data=>dispatch(createEntrySuccess(data)))
            .catch(err=>dispatch(createEntryFail(err)))
    };
}
const createFileStart = () => {
    return {
        type: actionType.CREATE_FILE_START
    };
};
const createFileSuccess = file => {
    return {
        type: actionType.CREATE_FILE_SUCCESS,
        file
    };
};
const createFileFail = error => {
    return {
        type: actionType.CREATE_FILE_FAIL,
        error
    };
};

export const createFile = (payload) => {
    const data = new FormData();
    data.append( 'file', payload, payload.name );
    return dispatch => {
        dispatch(createFileStart());
        axios.post('/api/upload', data,{
        headers: {
            'accept': 'application/json',
            'Accept-Language': 'en-US,en;q=0.8',
            'Content-Type': `multipart/form-data;`
        }
    })
            .then(res=>dispatch(createFileSuccess(res)))
            .catch(err=>dispatch(createFileFail(err)))
    };
}