import React, { Component } from "react";
import Summary from "../components/Summary/Summary";
import Table from "../components/Table/Table";
import "./Matrix.css";
import { fetchMatrixInfo, createEntry, createFile } from "../store/actions/matrix";
import { connect } from "react-redux";
import Modal from "../components/UI/Modal/Modal";
import Input from "../components/UI/Input/Input";

class Matrix extends Component {
  state = {
    title: "New title",
    columns: [{label: "Col 1" },{label: "Col 2" }],
    rows: [{label: "Row 1" }, {label: "Row 2" }],
    openFileInput: false,
    selectedInputFile: null 
  };

  componentDidMount() {
    this.props.onFetchMatrix();
  }

  addRow = () => {
    let newRows = this.state.rows.concat({
      label: `Row ${this.state.rows.length + 1}`
    });
    this.setState({ rows: newRows });
  };

  addCol = () => {
    let newCols = this.state.columns.concat({
      label: `Col ${this.state.columns.length + 1}`
    });
    this.setState({ columns: newCols });
  };

  removeRow = () => this.setState({ rows: this.state.rows.slice(0,-1) });

  removeCol = () => this.setState({ columns: this.state.columns.slice(0,-1) });
  
  handleChangeTitle = e => this.setState({title: e.target.value});

  handleChangeInput = (e, form, id) => {
    let input = this.state[form];
    input[id].label = e.target.value;

    this.setState({ [form]: input });
    };
  handleAnswer = (colId, rowId)=>{
    let input = this.state.rows;
    input[rowId].value = colId;
    this.setState({rows: input})
  } 
  handleUploadFile = (e, form, id) => {
    let input = this.state[form];
    input[id].fileName = e.target.files[0].name;
    this.setState({ [form]:input, openFileInput: false });
    this.props.onCreateFile(e.target.files[0], form, id);
  };

  openInputModal = (form, id) => {
    this.setState({ openFileInput: true, selectedInputFile: {form, id} })
  };

  closeModal = () => this.setState({ openFileInput: false });

  createEntry = () => {
    this.props.onCreateEntry({
      columns: this.state.columns,
      rows: this.state.rows,
      title: this.state.value
    });
  };

  render() {
    return (
      <div className="Content">
          <Modal show={this.state.openFileInput} modalClosed={this.closeModal}>
            <Input name='file' handleChangeInput={(e)=>this.handleUploadFile(e, this.state.selectedInputFile.form, this.state.selectedInputFile.id)} type="file" />
          </Modal>
        <Table
          title={this.state.title}
          rows={this.state.rows}
          columns={this.state.columns}
          addRow={this.addRow}
          addCol={this.addCol}
          removeRow={this.removeRow}
          removeCol={this.removeCol}
          createEntry={this.createEntry}
          openInputModal={this.openInputModal}
          handleChangeInput={this.handleChangeInput}
          handleChangeLabel={this.handleChangeLabel}
          handleAnswer={this.handleAnswer}
        />
        <Summary rows={this.state.rows} columns={this.state.columns} />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    columns: state.matrix.columns,
    rows: state.matrix.rows
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchMatrix: () => dispatch(fetchMatrixInfo()),
    onCreateEntry: payload => dispatch(createEntry(payload)),
    onCreateFile: file => dispatch(createFile(file))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Matrix);
