import React, { Component } from 'react';
import Matrix from "./containers/Matrix";
class App extends Component {
  render() {
    return (
      <div className="App">
        <Matrix/>
      </div>
    );
  }
}

export default App;
